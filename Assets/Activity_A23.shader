Shader "Actividad A.2.3" {
	Properties{
		_Color("Color", Color) = (1, 1, 1, 1)
	}
		SubShader{ // unity elige de cualquiera de los subshaders el que mejor determine que le queda a tu GPU

			Pass { // dependiendo el shader puede ser que se necesite procesar varias veces

				// aqu� es donde embebemos la l�gica en alg�n lenguaje de shaders
				// LENGUAJE QUE ESTAMOS USANDO SE LLAMA CG
				CGPROGRAM

				// vamos a especificar nombres de funciones para vertex y fragment shader
				#pragma vertex vert
				#pragma fragment frag

				uniform float4 _Color;

			// declaremos la funci�n del vertex shader
			float4 vert(float4 vertexPos : POSITION) : SV_POSITION
			{

			// para la actividad que qued� de tarea
			// float4 desplazado = float4(cos((_Time.y + vertexPos.z) * 5) + vertexPos.x, vertexPos.y, vertexPos.z, vertexPos.w);
			float4 desplazado = float4(vertexPos.x, vertexPos.y + cos((_Time.y + vertexPos.z) * 5) , vertexPos.z, vertexPos.w);
			return UnityObjectToClipPos(desplazado);

			}

				float4 frag(void) : COLOR
			{

				return _Color;
			//return float4(157/255.0, 252/255.0, 3/255.0, 1.0);
		}

		ENDCG

	}
	}
}



