Shader "Clase de Octubre 19" {

	Properties{
		_Color("Color", Color) = (1, 1, 1, 1)
		_Escala("Escala", Float) = 1
	}

		SubShader{ // unity elige de cualquiera de los subshaders el que mejor determine que le queda a tu GPU

			Pass { // dependiendo el shader puede ser que se necesite procesar varias veces

				// aqu� es donde embebemos la l�gica en alg�n lenguaje de shaders
				// LENGUAJE QUE ESTAMOS USANDO SE LLAMA CG
				CGPROGRAM

				// vamos a especificar nombres de funciones para vertex y fragment shader
				#pragma vertex vert
				#pragma fragment frag

				uniform float4 _Color;
				uniform float _Escala;

				// modificando tipos de llegada y salida
				struct input {
					float4 vertex : POSITION;
					float3 normal : NORMAL;
				};

				struct output {
					float4 pos : SV_POSITION;
					float4 col : COLOR;
				};

				// declaremos la funci�n del vertex shader
				output vert(input entrada)
				{
					output resultado;
					float4 modificado = entrada.vertex + float4(entrada.normal, 0) * _Escala;
					resultado.pos = UnityObjectToClipPos(modificado);
					resultado.col = float4(entrada.normal, 1);

					return resultado;

				}

				float4 frag(output entrada) : COLOR
				{
					//return entrada.col;
					return _Color;
				}

		ENDCG

	}
	}
}



