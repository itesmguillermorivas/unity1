Shader "Shadercito Basico" {
	SubShader{ // unity elige de cualquiera de los subshaders el que mejor determine que le queda a tu GPU

		Pass { // dependiendo el shader puede ser que se necesite procesar varias veces

			// aqu� es donde embebemos la l�gica en alg�n lenguaje de shaders
			// LENGUAJE QUE ESTAMOS USANDO SE LLAMA CG
			CGPROGRAM

			// vamos a especificar nombres de funciones para vertex y fragment shader
			#pragma vertex vert
			#pragma fragment frag

			// declaremos la funci�n del vertex shader
			float4 vert(float4 vertexPos : POSITION) : SV_POSITION
			{

				// float4 - vector tama�o 4 de floats
				// cada valor tiene 2 "definidores"	- tipo, semantic
				// tipo define c�mo se guarda en memoria
				// semantic define qu� representa / significa

				// recibimos los vertices en coordenadas LOCALES

				// UnityObjectToClipPos - aplica una serie de transformaciones que lo colocan en la posici�n que le corresponde
				// inclusive considerando c�mara
				float4 resultado = UnityObjectToClipPos(vertexPos);
				return float4(resultado.x + cos(_Time.y), resultado.y, resultado.z, resultado.w);

				// en un float4 podemos acceder a cada elemento por medio de 
				// x, y, z, w
				// r, g, b, a

			}

			float4 frag(void) : COLOR
			{

				return float4(0.0, 1.0, 0.0, 1.0);
			}

			ENDCG
	
		}	
	}
}



