Shader "Clase/Tarea-Nov9"
{
    Properties
    {
        _MaterialAmbiental("Color Ambiental", Color) = (1,1,1,1)
        _MaterialDifuso("Color Difuso", Color) = (1,1,1,1)
    }
        SubShader
    {
        Pass {
            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            uniform float4 _MaterialAmbiental;
            uniform float4 _MaterialDifuso;
            uniform float4 _LightColor0;

            struct vInput {

                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct vOutput {

                float3 normal: NORMAL;
                float4 pos : SV_POSITION;
            };

            vOutput vert(vInput input) {
            
                

                // generando el struct resultado
                vOutput resultado;
                resultado.pos = UnityObjectToClipPos(input.vertex);
                resultado.normal = input.normal;


                return resultado;
            }

            float4 frag(vOutput input) : COLOR{

                // primero - ambiental
                float4 ambiental = _MaterialAmbiental * _LightColor0 * 0.2f;

                // luego difuso
                float4 kd = _MaterialDifuso;
                float4 id = _LightColor0;

                // IMPORTANTE MUY IMPORTANTE
                // - recordar tener nuestros vectores en el mismo espacio
                // (osea todos locales o todos globales)
                // - recordar que los vectores que se utilicen para 
                // operaciones como producto punto est�n normalizados
                float3 lm = normalize(_WorldSpaceLightPos0.xyz);
                float3 n = UnityObjectToWorldNormal(input.normal);

                float4 difuso = kd * id * max(0.0, dot(lm, n));

                // calcular la intensidad del color para este punto
                float gris = (difuso.r + difuso.g + difuso.b) / 3;

                // esto s�lo se usa en el toon
                if (gris < 0.25f) {
                    difuso = float4(0, 0, 0, 1);
                }
                else if (gris < 0.50f) {
                    difuso = float4(_MaterialDifuso.r * 0.5f, _MaterialDifuso.g * 0.5f, _MaterialDifuso.b * 0.5f, 1);
                }
                else {
                    difuso = _MaterialDifuso;
                }

                // al final especular
                float4 especular = float4(0, 0, 0, 0);

                // usa esta linea para el toon
                // return ambiental + difuso + especular;

                // shader blanco y negro
                if (gris < 0.5f) {
                    return float4(0, 0, 0, 1);
                }
                else {
                    return float4(1, 1, 1, 1);
                }
            }

            ENDCG    
        }
        
    }
    FallBack "Diffuse"
}
