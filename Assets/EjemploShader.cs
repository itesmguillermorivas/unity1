using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EjemploShader : MonoBehaviour
{
    public Color colorcito;
    private Renderer renderer;

    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        renderer.material.SetColor("_Color", colorcito);
    }
}
