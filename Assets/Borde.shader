Shader "Custom/Borde"
{
    Properties
    {
        _Anchura("anchura", Float) = 0.03
    }
    SubShader
    {
        Pass {

        Stencil {
            Ref 1
            Comp Always
            Pass Replace
        }

        CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            float4 vert(float4 vertex : POSITION) : SV_POSITION{
                return UnityObjectToClipPos(vertex);
            }

            float4 frag(void) : COLOR {

                return float4(1.0, 0.0, 0.0, 1.0);
            }

        ENDCG

        }

        Pass {

        Cull off

        Stencil {
            Ref 1
            Comp NotEqual
            Pass Keep
        }
        CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            uniform float _Anchura;

            float4 vert(float4 vertex : POSITION, float3 normal : NORMAL) : SV_POSITION{
                return UnityObjectToClipPos(vertex + normal * _Anchura);
            }

            float4 frag(void) : COLOR {

                return float4(0.0, 0.0, 0.0, 1.0);
            }

        ENDCG

        }
        
    }
    FallBack "Diffuse"
}
