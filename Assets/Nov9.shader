Shader "Clase/Nov9"
{
    Properties
    {
        _MaterialAmbiental("Color Ambiental", Color) = (1,1,1,1)
        _MaterialDifuso("Color Difuso", Color) = (1,1,1,1)
    }
        SubShader
    {
        Pass {
            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            uniform float4 _MaterialAmbiental;
            uniform float4 _MaterialDifuso;
            uniform float4 _LightColor0;

            struct vInput {

                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct vOutput {

                float4 pos : SV_POSITION;
                float4 color : COLOR;
            };

            vOutput vert(vInput input) {
            
                // primero - ambiental
                float4 ambiental = _MaterialAmbiental * _LightColor0 * 0.2f;

                // luego difuso
                float4 kd = _MaterialDifuso;
                float4 id = _LightColor0;

                // IMPORTANTE MUY IMPORTANTE
                // - recordar tener nuestros vectores en el mismo espacio
                // (osea todos locales o todos globales)
                // - recordar que los vectores que se utilicen para 
                // operaciones como producto punto est�n normalizados
                float3 lm = normalize(_WorldSpaceLightPos0.xyz);
                float3 n = UnityObjectToWorldNormal(input.normal);

                float4 difuso = kd * id * max(0.0, dot(lm, n));

                // al final especular
                float4 especular = float4(0, 0, 0, 0);

                // generando el struct resultado
                vOutput resultado;
                resultado.pos = UnityObjectToClipPos(input.vertex);
                resultado.color = ambiental + difuso + especular;

                // escala de grises
                // para calcular gris sacamos promedio de todos los canales de color
                // float gris = (resultado.color.r + resultado.color.g + resultado.color.b) / 3;
                // resultado.color = float4(gris, gris, gris, 1);

                return resultado;
            }

            float4 frag(vOutput input) : COLOR{

                return input.color;
            }

            ENDCG    
        }
        
    }
    FallBack "Diffuse"
}
