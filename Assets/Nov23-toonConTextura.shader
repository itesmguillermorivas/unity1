Shader "Clase/Revision de tarea Toon con textura"
{
    Properties
    {
        _MaterialAmbiental("Color Ambiental", Color) = (1,1,1,1)
        _MaterialDifuso("Color Difuso", Color) = (1,1,1,1)
        _Tex("texturita chida", 2D) = "white" {}
    }
        SubShader
    {
        Pass {
            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            uniform float4 _MaterialAmbiental;
            uniform float4 _MaterialDifuso;
            uniform float4 _LightColor0;
            uniform sampler2D _Tex;

            struct vInput {

                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 coord : TEXCOORD0;
            };

            struct vOutput {

                float4 pos : SV_POSITION;
                float4 color : COLOR;
                float3 normal : NORMAL;
                float4 coord : TEXCOORD0;
            };

            vOutput vert(vInput input) {
            
                
                 
                

                // generando el struct resultado
                vOutput resultado;
                resultado.pos = UnityObjectToClipPos(input.vertex);
                resultado.normal = input.normal;
                resultado.coord = input.coord;
                resultado.color = float4(0, 0, 0, 1);

                // escala de grises
                // para calcular gris sacamos promedio de todos los canales de color
                // float gris = (resultado.color.r + resultado.color.g + resultado.color.b) / 3;
                // resultado.color = float4(gris, gris, gris, 1);

                return resultado;
            }

            float4 frag(vOutput input) : COLOR{

                // primero - ambiental
                float4 ambiental = _MaterialAmbiental * _LightColor0 * 0.2f;

                // luego difuso
                float4 kd = _MaterialDifuso;
                float4 id = _LightColor0;

                // IMPORTANTE MUY IMPORTANTE
                // - recordar tener nuestros vectores en el mismo espacio
                // (osea todos locales o todos globales)
                // - recordar que los vectores que se utilicen para 
                // operaciones como producto punto est�n normalizados
                float3 lm = normalize(_WorldSpaceLightPos0.xyz);
                float3 n = UnityObjectToWorldNormal(input.normal);

                float4 difuso = kd * id * max(0.0, dot(lm, n));

                // calcular intensidad del pixel
                float gris = (difuso.r + difuso.g + difuso.b) / 3;


                // obtener factor seg�n nivel de iluminaci�n
                float nivel = 0;

                if (gris < 0.30f) {
                    nivel = 0;
                }
                else if (gris < 0.60f) {
                    nivel = 0.5f;
                }
                else {
                    nivel = 1;
                }


                difuso = kd * id * nivel;

                // al final especular
                float4 especular = float4(0, 0, 0, 0);

                // m�todo para regresar el color espec�fico en una coordenada
                float4 colorTextura = tex2D(_Tex, input.coord.xy);

                // return colorTextura;
                return (ambiental + difuso + especular) * colorTextura;

                
            }

            ENDCG    
        }
        
    }
    FallBack "Diffuse"
}
