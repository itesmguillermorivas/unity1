using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActividadEnClase : MonoBehaviour
{

    public float escala;
    private Renderer renderer;

    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<Renderer>();    
    }

    // Update is called once per frame
    void Update()
    {
        renderer.material.SetFloat("_Escala", escala);
    }
}
