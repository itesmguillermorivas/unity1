Shader "Clase/Revision de tarea bump mapping"
{
    Properties
    {
        _MaterialAmbiental("Color Ambiental", Color) = (1,1,1,1)
        _MaterialDifuso("Color Difuso", Color) = (1,1,1,1)
        _MaterialEspecular("Color Especular", Color) = (1, 1, 1, 1)
        _Shininess("Shininess", Float) = 100
        _Tex("texturita chida", 2D) = "white" {}
        _Normal("mapa de normales", 2D) = "white" {}
    }
        SubShader
    {
        Pass {
            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            uniform float4 _MaterialAmbiental;
            uniform float4 _MaterialDifuso;
            uniform float4 _MaterialEspecular;
            uniform float _Shininess;

            uniform float4 _LightColor0;
            uniform sampler2D _Tex;
            uniform sampler2D _Normal;

            struct vInput {

                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 coord : TEXCOORD0;
            };

            struct vOutput {

                float4 vertex : TEXCOORD1;
                float3 normal: NORMAL;
                float4 pos : SV_POSITION;
                float4 coord : TEXCOORD0;
            };

            vOutput vert(vInput input) {
            
                

                // generando el struct resultado
                vOutput resultado;
                resultado.pos = UnityObjectToClipPos(input.vertex);
                resultado.normal = input.normal;
                resultado.coord = input.coord;
                resultado.vertex = input.vertex;


                return resultado;
            }

            float4 frag(vOutput input) : COLOR{

                // C�LCULO PARA AMBIENTAL
                
                // primero - ambiental
                float4 ambiental = _MaterialAmbiental * _LightColor0 * 0.2f;

                // C�LCULO PARA DIFUSO
                
                // luego difuso
                float4 kd = _MaterialDifuso;
                float4 id = _LightColor0;

                // IMPORTANTE MUY IMPORTANTE
                // - recordar tener nuestros vectores en el mismo espacio
                // (osea todos locales o todos globales)
                // - recordar que los vectores que se utilicen para 
                // operaciones como producto punto est�n normalizados
                float3 lm = normalize(_WorldSpaceLightPos0.xyz);
                //float3 n = UnityObjectToWorldNormal(input.normal);
                float3 n = normalize(tex2D(_Normal, input.coord.xy).xyz);

                float4 difuso = kd * id * max(0.0, dot(lm, n));
                
                // C�LCULO PARA ESPECULAR
                
                // los f�ciles
                float4 ks = _MaterialEspecular;
                float4 is = _LightColor0;
                float a = _Shininess;

                // calculando el vector luz reflejada
                // 1 obtener direcci�n a luz
                float3 l = normalize(_WorldSpaceLightPos0.xyz);
                float3 r = reflect(-l, UnityObjectToWorldNormal(input.normal));

                // calculando el vector hacia vista 
                // para obtener vector necesitamos restar 
                // posicion camara - posici�n de vertice (EN ESPACIO GLOBAL)

                // 1ero - posicion de vertex en espacio global
                float3 vertexGlobal = mul(unity_ObjectToWorld, input.vertex).xyz;
                float3 v = normalize(_WorldSpaceCameraPos - vertexGlobal);
                

                // al final especular
                float4 especular = ks * is * pow(max(0.0, dot(r, v)), a);

                float4 phong = ambiental + difuso + especular;

                //return tex2D(_Tex, input.coord.xy);
                return phong * tex2D(_Tex, input.coord.xy);

                // regresa float4
                // recuerda que son n�meros, se puede interpretar como color
                // o vector
                //return 
                /*
                // calcular la intensidad del color para este punto
                float gris = (difuso.r + difuso.g + difuso.b) / 3;


                
                // esto s�lo se usa en el toon
                if (gris < 0.25f) {
                    difuso = float4(0, 0, 0, 1);
                }
                else if (gris < 0.50f) {
                    difuso = float4(_MaterialDifuso.r * 0.5f, _MaterialDifuso.g * 0.5f, _MaterialDifuso.b * 0.5f, 1);
                }
                else {
                    difuso = _MaterialDifuso;
                }

                

                // usa esta linea para el toon
                // return ambiental + difuso + especular;

                // shader blanco y negro
                if (gris < 0.5f) {
                    return float4(0, 0, 0, 1);
                }
                else {
                    return float4(1, 1, 1, 1);
                }
                */
            }

            ENDCG    
        }
        
    }
    FallBack "Diffuse"
}
